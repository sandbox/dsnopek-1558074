<?php

/**
 * A handler to provide a the current nodes comment form.
 *
 */
class views_comment_form_field_handler extends views_handler_field {
  function construct() {
    parent::construct();
    $this->additional_fields['nid'] = 'nid';
    $this->additional_fields['type'] = 'type';
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    $nid = $this->get_value($values, 'nid');
    $type = $this->get_value($values, 'type');
    return drupal_get_form("comment_node_{$type}_form", (object)array('nid' => $nid, 'pid' => NULL));
  }
}

