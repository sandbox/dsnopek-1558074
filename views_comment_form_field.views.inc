<?php

/**
 * @file
 * Provide views handlers and plugins that allow usage of comment form.
 */

/**
 * Implements hook_views_data().
 */
function views_comment_form_field_views_data() {
  $data['node']['comment_form'] = array(
    'title' => t('Comment form'),
    'help' => t('Use comment form.'),
    'field' => array(
      'click sortable' => FALSE,
      'help' => t('Show comment form for this field.'),
      'handler' => 'views_comment_form_field_handler',
    ),
  );
  return $data;
}

